
public class Brute {
  public static void main(String[] args) {
    if (args.length == 0)
      throw new IllegalArgumentException();

    StdDraw.setXscale(0, 32768);
    StdDraw.setYscale(0, 32768);

    In in = new In(args[0]);
    int N = in.readInt();
    Point[] points = new Point[N];

    for (int i = 0; i < N && !in.isEmpty(); ++i) {
      int x = in.readInt();
      int y = in.readInt();
      points[i] = new Point(x, y);
      StdDraw.setPenColor(StdDraw.BLACK);
      points[i].draw();
    }

    for (int i = 0; i < points.length; ++i)
      if (points[i] == null)
        throw new IllegalArgumentException();

    for (int i = 0; i < points.length-3; ++i) {
      Point p0 = points[i];
      for (int j = i+1; j < points.length-2; ++j) {
        Point p1 = points[j];
        double slope0 = p0.slopeTo(p1);
        for (int k = j+1; k < points.length-1; ++k) {
          if (k == i || k == j)
            continue;
          Point p2 = points[k];
          double slope1 = p1.slopeTo(p2);
          if (slope1 != slope0)
            continue;
          for (int l = k+1; l < points.length; ++l) {
            if (l == k || l == j || l == i)
              continue;
            Point p3 = points[l];
            double slope2 = p2.slopeTo(p3);
            if (slope2 != slope0)
              continue;

            Point[] segment = new Point[4];
            segment[0] = p0;
            segment[1] = p1;
            segment[2] = p2;
            segment[3] = p3;
            java.util.Arrays.sort(segment);

            StdOut.println(segment[0]+" -> "+segment[1]+" -> "+
                           segment[2]+" -> "+segment[3]);

            StdDraw.setPenColor(StdDraw.BLUE);
            segment[0].drawTo(segment[3]);
          }
        }
      }
    }

    StdDraw.show(0);
    StdDraw.setPenRadius();
  }
}
