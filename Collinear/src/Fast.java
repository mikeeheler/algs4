
public class Fast {
  public static void main(String[] args) {
    if (args.length == 0)
      throw new IllegalArgumentException();

    StdDraw.setXscale(0, 32768);
    StdDraw.setYscale(0, 32768);

    In in = new In(args[0]);
    int N = in.readInt();
    Point[] points = new Point[N];

    for (int i = 0; i < N && !in.isEmpty(); ++i) {
      int x = in.readInt();
      int y = in.readInt();
      points[i] = new Point(x, y);
      StdDraw.setPenColor(StdDraw.BLACK);
      points[i].draw();
    }

    for (int i = 0; i < points.length; ++i)
      if (points[i] == null)
        throw new IllegalArgumentException();

    StdDraw.setPenColor(StdDraw.BLUE);

    for (int i = 0; i < points.length-3; ++i) {
      Point p = points[i];
      java.util.Arrays.sort(points, i+1, points.length, p.SLOPE_ORDER);

      int sequenceStart = i+1;
      int sequenceEnd = i+1;
      int numInSequence = 0;
      double activeSlope = Double.NEGATIVE_INFINITY;

      for (int j = i+1; j < points.length; ++j) {
        double slope = p.slopeTo(points[j]);
        if (slope == activeSlope) {
          sequenceEnd = j;
          ++numInSequence;
          continue;
        }

        if (numInSequence >= 3) {
          Point[] line = new Point[numInSequence+1];
          line[0] = p;

          for (int k = sequenceStart; k <= sequenceEnd; ++k)
            line[k-sequenceStart+1] = points[k];

          java.util.Arrays.sort(line);
          line[0].drawTo(line[line.length-1]);

          for (int k = 0; k < line.length; ++k) {
            StdOut.print(line[k]);
            if (k < line.length-1)
              StdOut.print(" -> ");
            else
              StdOut.println();
          }
        }

        sequenceStart = j;
        numInSequence = 1;
        activeSlope = slope;
      }
    }
  }
}
