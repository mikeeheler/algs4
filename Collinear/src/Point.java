import java.util.Comparator;

public class Point implements Comparable<Point> {
  public final Comparator<Point> SLOPE_ORDER = new BySlope();

  private final int x;
  private final int y;

  public Point(int x, int y) {
    this.x = x;
    this.y = y;
  }

  public void draw() {
    StdDraw.point(x, y);
  }

  public void drawTo(Point that) {
    StdDraw.line(this.x, this.y, that.x, that.y);
  }

  public double slopeTo(Point that) {
    if (that == null)
      throw new NullPointerException();

    if (this.y == that.y) return 0;
    if (this.x == that.x) return Double.POSITIVE_INFINITY;
    if (compareTo(that) == 0) return Double.NEGATIVE_INFINITY;

    return (double)(that.y - this.y) / (that.x - this.x);
  }

  public int compareTo(Point that) {
    if (that == null)
      throw new NullPointerException();

    if (this.y < that.y) return -1;
    if (this.y > that.y) return 1;
    if (this.x < that.x) return -1;
    if (this.x > that.x) return 1;

    return 0;
  }

  public String toString() {
    return "(" + x + ", " + y + ")";
  }

  private class BySlope implements Comparator<Point> {
    public int compare(Point p0, Point p1) {
      if (p0 == null || p1 == null)
        throw new NullPointerException();

      double slope0 = slopeTo(p0);
      double slope1 = slopeTo(p1);

      if (slope0 < slope1) return -1;
      if (slope0 > slope1) return 1;

      return 0;
    }
  }

  public static void main(String[] args) {
    Point p00 = new Point(0,0);
    assert p00.slopeTo(p00) == Double.NEGATIVE_INFINITY;
    Point p01 = new Point(0,1);
    assert p00.slopeTo(p01) == Double.POSITIVE_INFINITY;
    Point p10 = new Point(1,0);
    assert p00.slopeTo(p10) == 0.0;
  }
}
