public class Percolation {
  private int N;
  private boolean[] openCell;
  private int size;
  private WeightedQuickUnionUF uf;
  private WeightedQuickUnionUF ufFull;

  public Percolation(int N) {
    if (N < 1)
      throw new IllegalArgumentException();

    this.N = N;
    this.size = N * N;
    this.openCell = new boolean[size + 2];
    this.uf = new WeightedQuickUnionUF(size + 2);
    this.ufFull = new WeightedQuickUnionUF(size + 1);

    openCell[0] = true;
    for (int i = 1; i <= size; ++i)
      openCell[i] = false;
    openCell[size + 1] = true;
  }

  public void open(int i, int j) {
    checkRange(i, j);

    int cellId = getId(i, j);

    int upId = getId(i-1, j);
    int downId = getId(i+1, j);

    if (i == 1)
      upId = 0;
    if (i == N)
      downId = size + 1;

    openCell[cellId] = true;

    if (openCell[upId]) {
      uf.union(cellId, upId);
      ufFull.union(cellId, upId);
    }

    if (openCell[downId]) {
      uf.union(cellId, downId);
      if (downId != size + 1)
        ufFull.union(cellId, downId);
    }

    if (j > 1) {
      int leftId = getId(i, j-1);
      if (openCell[leftId]) {
        uf.union(cellId, leftId);
        ufFull.union(cellId, leftId);
      }
    }

    if (j < N) {
      int rightId = getId(i, j+1);
      if (openCell[rightId]) {
        uf.union(cellId, rightId);
        ufFull.union(cellId, rightId);
      }
    }
  }

  public boolean isOpen(int i, int j) {
    checkRange(i, j);

    return openCell[getId(i, j)];
  }

  public boolean isFull(int i, int j) {
    checkRange(i, j);
    int id = getId(i, j);
    return openCell[id] && ufFull.connected(0, id);
  }

  public boolean percolates() {
    return uf.connected(0, size + 1);
  }

  private void checkRange(int i, int j) {
    if (i < 1 || i > N || j < 1 || j > N)
      throw new IndexOutOfBoundsException();
  }

  private int getId(int i, int j) {
    return (i-1) * N + j;
  }

  public static void main(String[] args) {
    int N = 3;
    Percolation p = new Percolation(N);
    StdOut.printf("getId(2,2) = %d (expected: 5)\n", p.getId(2, 2));

    boolean anyOpen = false;
    for (int row = 1; !anyOpen && row <= N; ++row) {
      for (int col = 1; !anyOpen && col <= N; ++col) {
        anyOpen &= p.isOpen(row, col);
      }
    }
    StdOut.printf("anyOpen: %b\n\n", anyOpen);

    StdOut.println("p.open(2,2):");
    p.open(2, 2);
    for (int row = 1; row <= N; ++row) {
      for (int col = 1; col <= N; ++col) {
        StdOut.printf("  isOpen(%d,%d): %b\n", row, col, p.isOpen(row, col));
      }
    }
    StdOut.println();

    StdOut.println("Test percolates():");
    p.open(3, 3);
    p.open(2, 3);
    p.open(1, 2);
    StdOut.printf("open(3-3,2-3,2-2,1-2): %b\n\n", p.percolates());

    p = new Percolation(N);
    p.open(1, 1);
    p.open(2, 2);
    p.open(3, 3);
    StdOut.printf("open(3-3,2-2,1-1): %b\n\n", p.percolates());
  }
}
