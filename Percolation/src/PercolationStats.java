public class PercolationStats {
  private double mu = 0.0;
  private double sigma = 0.0;
  private double confidenceLo = 0.0;
  private double confidenceHi = 0.0;

  public PercolationStats(int N, int T) {
    if (N <= 0 || T <= 0)
      throw new IllegalArgumentException();

    int[] opened = new int[T];

    // Run experiment
    for (int i = 0; i < T; ++i) {
      Percolation p = new Percolation(N);

      while (!p.percolates()) {
        int row = StdRandom.uniform(1, N+1);
        int col = StdRandom.uniform(1, N+1);
        while (p.isOpen(row, col)) {
          row = StdRandom.uniform(1, N+1);
          col = StdRandom.uniform(1, N+1);
        }
        p.open(row, col);
        ++opened[i];
      }
    }

    // Calculate mean
    double sum = 0;
    for (int i = 0; i < T; ++i) {
      sum += (double) opened[i] / (N*N);
    }
    mu = sum / T;

    // Calculate stddev
    double sigmaSquared = 0.0;
    for (int i = 0; i < T; ++i) {
      double value = (double) opened[i] / (N*N) - mu;
      sigmaSquared += value * value;
    }
    sigmaSquared /= (T - 1);
    sigma = Math.sqrt(sigmaSquared);

    // Calculate confidence
    double interval = (1.96 * sigma) / Math.sqrt((double) T);
    confidenceLo = mu - interval;
    confidenceHi = mu + interval;
  }

  public double mean() {
    return this.mu;
  }

  public double stddev() {
    return this.sigma;
  }

  public double confidenceLo() {
    return this.confidenceLo;
  }

  public double confidenceHi() {
    return this.confidenceHi;
  }

  public static void main(String[] args) {
    int N = Integer.parseInt(args[0]);
    int T = Integer.parseInt(args[1]);

    PercolationStats pstats = new PercolationStats(N, T);

    StdOut.print("mean                    = ");
    StdOut.println(pstats.mean());
    StdOut.print("stddev                  = ");
    StdOut.println(pstats.stddev());
    StdOut.print("95% confidence interval = ");
    StdOut.print(pstats.confidenceLo());
    StdOut.print(", ");
    StdOut.println(pstats.confidenceHi());
  }
}
