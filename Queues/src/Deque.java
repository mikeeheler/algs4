import java.util.Iterator;

public class Deque<Item> implements Iterable<Item> {
  private int count = 0;
  private Node<Item> pre = null;
  private Node<Item> post = null;

  public Deque() {
    pre = new Node<Item>(null);
    post = new Node<Item>(null);
    pre.next = post;
    post.previous = pre;
  }

  public boolean isEmpty() {
    return count == 0;
  }

  public int size() {
    return count;
  }

  public void addFirst(Item item) {
    if (item == null)
      throw new NullPointerException();

    Node<Item> node = new Node<Item>(item);
    node.previous = pre;
    node.next = pre.next;
    pre.next.previous = node;
    pre.next = node;

    ++count;
  }

  public void addLast(Item item) {
    if (item == null)
      throw new NullPointerException();

    Node<Item> node = new Node<Item>(item);
    node.previous = post.previous;
    node.next = post;
    post.previous.next = node;
    post.previous = node;

    ++count;
  }

  public Item removeFirst() {
    if (isEmpty())
      throw new java.util.NoSuchElementException();

    Item item = pre.next.item;

    pre.next = pre.next.next;
    pre.next.previous = pre;
    --count;

    return item;
  }

  public Item removeLast() {
    if (isEmpty())
      throw new java.util.NoSuchElementException();

    Item item = post.previous.item;
    post.previous = post.previous.previous;
    post.previous.next = post;
    --count;

    return item;
  }

  @Override
  public Iterator<Item> iterator() {
    return new DequeIterator();
  }

  public static void main(String[] args) {
    Deque<String> dt = new Deque<String>();
    dt.addFirst("First");
    dt.addFirst("Second");
    for (String s : dt) {
      StdOut.println(s);
    }
    StdOut.println("\nRemoveFirst");
    dt.removeFirst();
    for (String s : dt) {
      StdOut.println(s);
    }

    String last = dt.removeLast();
    StdOut.println(last);
    StdOut.println();

    if (dt.isEmpty())
      StdOut.println("EMPTY");

    dt.addFirst("Test");
    StdOut.println(dt.removeLast()); // Test
    dt.addLast("Test2");
    StdOut.println(dt.removeFirst()); // Test2
    dt.addLast("Test3");
    dt.addFirst("Test4");
    StdOut.println(dt.removeLast()); // Test3
    StdOut.println(dt.removeFirst()); // Test4
    dt.addLast("Test5");
    dt.addFirst("Test6");
    StdOut.println(dt.removeFirst()); // Test6
    StdOut.println(dt.removeLast()); // Test5
    dt.addFirst("Test7");
    dt.addLast("Test8");
    StdOut.println(dt.removeLast()); // Test8
    StdOut.println(dt.removeFirst()); // Test7
    dt.addFirst("Test9");
    dt.addLast("Test10");
    StdOut.println(dt.removeFirst()); // Test9
    StdOut.println(dt.removeLast()); // Test10
    StdOut.println();

    Deque<String> dt2 = new Deque<String>();
    dt2.addFirst("Blah");
    dt2.addFirst("di");
    dt2.addFirst("blah");
    java.util.Iterator<String> iter = dt2.iterator();
    while (iter.hasNext())
      StdOut.println(iter.next());
  }

  private static class Node<Item> {
    private Item item = null;
    private Node<Item> previous = null;
    private Node<Item> next = null;

    public Node(Item x) {
      item = x;
    }
  }

  private class DequeIterator implements Iterator<Item> {
    private Node<Item> current = pre.next;

    public boolean hasNext() {
      return current != post;
    }

    public void remove() {
      throw new UnsupportedOperationException();
    }

    public Item next() {
      if (!hasNext())
        throw new java.util.NoSuchElementException();

      Item item = current.item;
      current = current.next;

      return item;
    }
  }
}
