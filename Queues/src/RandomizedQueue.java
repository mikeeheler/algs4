import java.util.Iterator;

public class RandomizedQueue<Item> implements Iterable<Item> {
  private Item[] items;
  private int count = 0;

  public RandomizedQueue() {
    items = (Item[]) new Object[1];
    count = 0;
  }

  public boolean isEmpty() {
    return count == 0;
  }

  public int size() {
    return count;
  }

  public void enqueue(Item item) {
    if (item == null)
      throw new NullPointerException();
    if (count >= items.length)
      resize(items.length * 2);
    items[count++] = item;
  }

  public Item dequeue() {
    if (isEmpty())
      throw new java.util.NoSuchElementException();

    int index = StdRandom.uniform(count);
    Item item = items[index];
    Item endItem = items[--count];
    items[index] = endItem;
    items[count] = null;

    if (count > 0 && count == items.length/4)
      resize(items.length/2);

    return item;
  }

  public Item sample() {
    if (isEmpty())
      throw new java.util.NoSuchElementException();

    int index = StdRandom.uniform(count);
    Item item = items[index];

    return item;
  }

  @Override
  public Iterator<Item> iterator() {
    return new RandomizedQueueIterator();
  }

  public static void main(String[] args) {
  }

  private void resize(int N) {
    Item[] newItems = (Item[]) new Object[Math.max(1, N)];
    for (int i = 0; i < Math.min(items.length, newItems.length); ++i)
      newItems[i] = items[i];
    items = newItems;
  }

  private class RandomizedQueueIterator implements Iterator<Item> {
    private int[] random;
    private int index;

    RandomizedQueueIterator() {
      random = new int[count];
      for (int i = 0; i < random.length; ++i)
        random[i] = i;
      for (int i = 1; i < random.length; ++i) {
        int r = StdRandom.uniform(i+1);
        int swap = random[i];
        random[i] = random[r];
        random[r] = swap;
      }
    }

    public boolean hasNext() {
      return index < random.length;
    }

    public void remove() {
      throw new UnsupportedOperationException();
    }

    public Item next() {
      if (!hasNext())
        throw new java.util.NoSuchElementException();
      return items[random[index++]];
    }
  }
}
