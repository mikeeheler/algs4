
public class TestRandomizedQueue {
  public static void main(String[] args) {
    test1();
    test2();
  }

  private static void test1() {
    RandomizedQueue<Integer> queue = new RandomizedQueue<Integer>();
    int[] values = new int[5];

    for (int i = 0; i < 5; ++i) {
      values[i] = StdRandom.uniform(100000);
      queue.enqueue(values[i]);
    }

    for (int i = 0; i < 5; ++i) {
      int value = queue.dequeue();
      for (int j = 0; j < values.length; ++j) {
        if (value == values[j]) {
          values[j] = -1;
          break;
        }
      }
    }

    int hanging_values = 0;
    for (int i = 0; i < values.length; ++i) {
      if (values[i] != -1)
        hanging_values++;
    }

    if (hanging_values > 0) {
      StdOut.print(hanging_values);
      StdOut.println(" values have been left hanging.");
    }
  }
  
  private static void test2() {
    int[] freq = new int[5];
    for (int i = 0; i < freq.length; ++i)
      freq[i] = 0;
    
    for (int i = 0; i < 5000; ++i) {
      RandomizedQueue<String> queue = new RandomizedQueue<String>();
      queue.enqueue("A");
      queue.enqueue("B");
      queue.enqueue("C");
      queue.enqueue("D");
      queue.enqueue("E");
      java.util.Iterator<String> iter = queue.iterator();
      
    }
  }
}
